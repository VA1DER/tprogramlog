//------------------------------ $Keywords ----------------------------------
// programlog.inc - TProgramLog class helper macros
// Copyright ©2003-2024, Kurt Fitzner <kurt@va1der.ca>
//---------------------------------------------------------------------------
// This file is part of the TProgramLog general purpose debug logging
// class, included in many projects.
//
// The TProgramLog class is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License, Version 3,
// as published by the Free Software Foundation.
//
// The TProgramLog class is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// File Notes:
//---------------------------------------------------------------------------
// 6 Oct 2023 - Kurt Fitzner <kurt@va1der.ca>
//
// These macros aren't as tidy to use as the old C++ Builder version, because
// FPC devs are alergic to proper macros and also because both {$I %FILE%}
// and {$I %LINE%} trigger the instant the compiler sees them, so are utterly
// useless for macros.  Use the macros as follows:
(*
  function TClass.Function(): Integer;
  begin
    __ENTERFUNCTION{$I %FILE%},{$I %LINE%}__
    // Do stuff
    __LEAVEFUNCTION{$I %FILE%},{$I %LINE%}__
  end;
*)
// If _DEBUG isn't $defined, or DISABLE_LOGGING is $defined, then these
// macros melt away to nothing without even call stubs.

{$ifdef _DEBUG and $ifndef DISABLE_LOGGING}
{$define __ENTERPROGRAM  :=Logger.LogEntry(LOG_STRUCTURE, '-> Log initialized in %s()', [{$I %CURRENTROUTINE%}], }
{$define __ENTERFUNCTION :=Logger.LogEntry(LOG_STRUCTURE, '-> Entering %s()', [{$I %CURRENTROUTINE%}], }
{$define __LEAVEFUNCTION :=Logger.LogEntry(LOG_STRUCTURE, '<- Leaving  %s()', [{$I %CURRENTROUTINE%}], }
{$define __RETURNFUNCTION:=Logger.LogEntry(LOG_STRUCTURE, '<- Leaving  %s()', [{$I %CURRENTROUTINE%}], }
{$define __LEAVEPROGRAM  :=Logger.LogEntry(LOG_STRUCTURE, '<- Terminating log in %s()', [{$I %CURRENTROUTINE%}], }
{$define __ := );}
{$else} // $ifndef _DEBUG or $ifdef DISABLE_LOGGING
{$define __ENTERPROGRAM  :=//}
{$define __ENTERFUNCTION :=//}
{$define __LEAVEFUNCTION :=//}
{$define __RETURNFUNCTION:=//}
{$define __LEAVEPROGRAM  :=//}
{$define __ :=}
{$endif}
{$define return := goto _return}
