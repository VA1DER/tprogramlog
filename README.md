# tprogramlog

A small, general purpose application logging class written in Pascal for freepascal/Lazarus projects.  It's not particularly pretty, and it could be more complete.  It is something I use in my projects, and gets features as my projects need them.

Probably not something the average developer would likely be interested in.  Nothing to see here.  Move along.  Move along.