//------------------------------ $Keywords ----------------------------------
// programlog.pas - TProgramLog class general purpose debug logger
// Copyright ©2003-2024, Kurt Fitzner <kurt@va1der.ca>
//---------------------------------------------------------------------------
// This file is part of the TProgramLog general purpose debug logging
// class, included in many projects.
//
// The TProgramLog class is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License, Version 3,
// as published by the Free Software Foundation.
//
// The TProgramLog class is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// File Notes:
//---------------------------------------------------------------------------
// 5 Oct 2023 - Kurt Fitzner <kurt@va1der.ca>
//
// A fairly generic logging class, designed to be versatile and to get out of
// your way if you only want it for debugging.  Ported from the class of the
// same name originally written in C++ Builder quite a few years ago.
//
// You can log right down to the entry/exit of subroutines by adding
// {$I programlog.inc} to each source file and using the __ENTERFUNCTION__
// and __LEAVEFUNCTION__ macros at the entry and exit of any function or
// procedure.  When _DEBUG is $defined, these will emit log messages at the
// lowest log level (LOG_STRUCTURE).  When _DEBUG is not defined, these macros
// expand to nothing, and thus incur no cost. See the note in programlog.inc
//
// When DISABLE_LOGGING is defined, the entire class almost entirely
// evaporates.  If Free Pascal ever supports inline functions with Array of
// Const (varargs) or proper macros (with arguments) then it will be able to
// completely evaporate.  As it stands, all that remains are function stubs.
//---------------------------------------------------------------------------
// 11 Oct 2023 - Kurt Fitzner <kurt@va1der.ca>
//
// Added the concept of a backlog.  You can now create the log at a log level
// of LOG_BACKLOG which will activate the backlog and will not write any log
// entries, but instead record them all internally.
//
// This is because you might want to create the logging object to capture
// debug events very early in the program's life; before you process what log
// level the user actually wants displayed.  Once the user's log level
// preference is specified and Log.LogLevel is set for the first time since
// the log is created, then it will write out all captured events at or above
// that log level, and will then cease back logging.
//---------------------------------------------------------------------------


unit ProgramLog;

{$mode ObjFPC}{$LONGSTRINGS ON}{$modeswitch duplicatelocals}{$MACRO ON}{$BOOLEVAL OFF}{$INLINE ON}

interface

uses
  Classes, SysUtils, CustApp, Dos;

type
  TLogLevels = (
    LOG_STRUCTURE = 0,    // Intended for debugging program flow and logging
    LOG_DEBUG     = 1,
    LOG_INFO      = 2,
    LOG_MESSAGE   = 3,
    LOG_WARNING   = 4,
    LOG_ERROR     = 5,
    LOG_CRITICAL  = 99,
    LOG_NONE      = 100,
    LOG_BACKLOG   = 1000 // Intended for initial creation of a log where the user sets the log level later
  );   // enum TLogLevels

  TProgramLog = class
  protected
    //FunctionStack: TStack;
    FBackLog:      TStringList;
    FFileHandle:   THandle;
    FFileName:     String;
    FSynchronous:  Boolean;
    FShowThreads:  Boolean;
    FDuplicate:    Boolean;
    FLogLevel:     TLogLevels;
    procedure      SetLogLevel(LogLevel: TLogLevels);
    //procedure    _LogEntry(Message: String; vargs: Array of const; SourceFile: String = ''; SourceLine: String = '');
    //procedure    _LogEntry(LogLevel: TLogLevels; Message: String; vargs: Array of const; SourceFile: String = ''; SourceLine: String = '');
  public
    MainThreadId:  TThreadId;
    constructor    Create(FileName: String; LogLevel: TLogLevels  = LOG_WARNING; ShowThreads: Boolean = True; Synchronous: Boolean = True; OverWrite: Boolean = False; {%H-}Duplicate: Boolean = False);
    constructor    Create(FileHandle: THandle; LogLevel: TLogLevels; ShowThreads: Boolean = True; Synchronous: Boolean = True; Duplicate: Boolean = False);
    destructor     Destroy; override;
    function       SetFile(FileName: String; OverWrite: Boolean = False): Boolean;
    procedure      LogEntry(Message: String; vargs: Array of const; SourceFile: String = ''; SourceLine: String = ''); // inline
    procedure      LogEntry(LogLevel: TLogLevels; Message: String; vargs: Array of const; SourceFile: String = ''; SourceLine: String = ''); // inline
    procedure      LogEntry(LogLevel: TLogLevels; Message: String; vargs: Array of const; Multi: TStrings; SourceFile: String = ''; SourceLine: String = ''); // inline
    procedure      LogEntry(LogLevel: TLogLevels; Message: String; vargs: Array of const; Multi: Array of String; SourceFile: String = ''; SourceLine: String = ''); // inline
    property       LogLevel: TLogLevels read FLogLevel write SetLogLevel;
    property       Synchronous: Boolean read FSynchronous write FSynchronous;
    property       ShowThreads: Boolean read FShowThreads write FShowThreads;
    property       Duplicate: Boolean read FDuplicate write FDuplicate;
  end;  // class TProgramLog

  function FriendlyThreadId: String;

var
  Logger: TProgramLog;
  Application: TCustomApplication;

implementation

//---------------------------------------------------------------------------
// TProgramLog.Create - Main class constructor
//
// Takes:   FileName:    String log filename - no real checking done
//          LogLevel:    TLogLevels enum with the lowest log level to allow
//          ShowThreads: Boolean flag for whether the thread ID should be
//                       displayed by default (implies Synchronous)
//          Synchronous: Boolean flag for using synchronous I/O writes
//          Overwrite:   Boolean flag for whether the log should be
//                       overwritten or appended
//          Duplicate:   If true then duplicate all entries to stdout.  No
//                       checking on whether or not it's already going there
// Throws:  Nothing
//
constructor TProgramLog.Create(FileName: String; LogLevel: TLogLevels; ShowThreads: Boolean; Synchronous: Boolean; OverWrite: Boolean; Duplicate: Boolean);
begin
  {$IFNDEF DISABLE_LOGGING}
  if (SetFile(FileName, OverWrite)) then
    Create(FFileHandle, LogLevel, ShowThreads, Synchronous)
  else
    fail;
  {$ENDIF}
end;  // constructor TProgramLog.Create(FileName: String; LogLevel: TLogLevels; Synchronous: Boolean; OverWrite: Boolean);
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// TProgramLog.Create - Alternate constructor to use with a pre-existing
//  file handle
// Takes:   FileHandle: Handle type with a pre-opened THandle file handle
//                      (ie: StdOutputHandle)
//          LogLevel, ShowThreads, Synchronous, and Duplicate: See above
// Throws:  Nothing
//
constructor TProgramLog.Create(FileHandle: THandle; LogLevel: TLogLevels; ShowThreads: Boolean; Synchronous: Boolean; Duplicate: Boolean);
begin
  inherited Create();
  {$IFNDEF DISABLE_LOGGING}
  //FunctionStack := TStack.Create;
  if (LogLevel = LOG_BACKLOG) then
    FBackLog := TStringList.Create
  else
    FBackLog := nil;
  Self.FShowThreads := ShowThreads;
  if ShowThreads then
    Self.FSynchronous := True
  else
    Self.FSynchronous := Synchronous;
  Self.FDuplicate := Duplicate;
  Self.FLogLevel := LogLevel;
  Self.FFileHandle := FileHandle;
  Self.MainThreadId:=GetCurrentThreadId();
  {$ENDIF}
end;  // constructor TProgramLog.Create(FileHandle: THandle; LogLevel: TLogLevels; Synchronous: Boolean);
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// TProgramLog.Free - C'est une destructor
//
// Throws: Nothing
//
destructor TProgramLog.Destroy;
begin
  {$IFNDEF DISABLE_LOGGING}
  //FreeAndNil(FunctionStack);
  if (FBackLog <> nil) then
    FreeAndNil(FBackLog);
  if (FFileHandle <> THandle(-1)) then begin
    FileFlush(FFileHandle);
    if (FFileHandle <> StdOutputHandle) then
      FileClose(FFileHandle);
  end;
  {$ENDIF}
  inherited;
end;  // destructor TProgramLog.Free;
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// TProgramLog.SetFile - Set the logging output to a file.  Useful in
//  conjunction with backlogging so you can create the logging object early
//  in the application's startup (before you know where the user wants the
//  log) and then set the file and turn off backlogging to flush the backlog
//  to the newly opened file
//
// Takes:   FileName:    String log filename - no real checking done
//          Overwrite:   Boolean flag for whether the log should be
//                       overwritten or appended
// Returns: Boolean True on success, False on error
// Throws:  Nothing
//
function TProgramLog.SetFile(FileName: String; OverWrite: Boolean = False): Boolean;
{$IFNDEF DISABLE_LOGGING}
var
  Handle: THandle;
  {$ENDIF}
begin
  {$IFNDEF DISABLE_LOGGING}
  FileName := FExpand(FileName);
  if (OverWrite) then
    DeleteFile(FileName);
  if (not FileExists(FileName)) then begin
    Handle := FileCreate(FileName);
    FileClose(Handle);
  end;  // if (not FileExists(FileName))
  Handle := FileOpen(FileName, fmOpenReadWrite or fmShareDenyWrite);
  if (Handle <> THandle(-1)) then begin
    FileSeek(Handle, 0, fsFromEnd);
    Self.FFileName := ExpandFileName(FileName);
    FFileHandle := Handle;
    Result := True;
  end  // if (Handle <> THandle(-1))
  else Result := False;
  {$ENDIF}
end;  // function TProgramLog.SetFile(FileName: String; OverWrite: Boolean = False): Boolean
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// TProgramLog.SetLogLevel - LogLevel property write procedure, sets the
//  log level.  If there is any backlog, then write it.
//
procedure TProgramLog.SetLogLevel(LogLevel: TLogLevels);
{$IFNDEF DISABLE_LOGGING}
var
  n: integer = 0;
  WasBackLogging: Boolean;
  EntryLevel: TLogLevels;
  Entry: String;
  {$ENDIF}
begin
  {$IFNDEF DISABLE_LOGGING}
  WasBackLogging := (FLogLevel = LOG_BACKLOG);
  FLogLevel := LogLevel;
  if (WasBackLogging) then begin
    while (n < FBackLog.Count) do begin
      EntryLevel := TLogLevels(StrToInt(FBackLog.Names[n]));
      if (EntryLevel >= LogLevel) then begin
        Entry := FBackLog.ValueFromIndex[n];
        FileWrite(FFileHandle, Pointer(Entry)^, Entry.Length);
      end;  // if (EntryLevel >= LogLevel)
      inc(n);
    end;  // while (n < FBackLog.Count)
  end;  // if (WasBackLogging)
  {$ENDIF}
end;  // procedure TProgramLog.SetLogLevel(LogLevel: TLogLevels);
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// TProgramLog.LogEntry() - Write an entry into the log.
//
// Takes:   LogLevel:  TLogLevels enum with the log level for this entry
//          Message:   String message with optional "printf" style format
//                     specifiers (see Format()),
//          vargs:     Array of const (Pascal varargs) with the arguments for
//                     the "printf",
//          SourceFile:Optional string for the source file specifier.
//                     (defaults to the application title)
//          SourceLine:Optional string with the source file line number.
//
//          SourceFile and SourceLine are intended to be used with FPC's
//          {$I %FILE%} and {$I %LINE%}
//
// Returns: Nothing
// Throws:  Nothing
//
procedure TProgramLog.LogEntry(LogLevel: TLogLevels; Message: String; vargs: Array of const; SourceFile: String = ''; SourceLine: String = '');
{$IFNDEF DISABLE_LOGGING}
var
  TimeStamp: String;
  Entry: AnsiString;
  {$ENDIF}
begin
  {$IFNDEF DISABLE_LOGGING}
  if (LogLevel >= Self.FLogLevel) or (FLogLevel = LOG_BACKLOG) then begin
    if (FFileHandle <> THandle(-1)) then begin
      if (SourceFile = '') then SourceFile := Application.Title;
      if (SourceLine = '') then SourceLine := '0';
      DateTimeToString(TimeStamp, 'ddmmmyyyy hh:nn:ss.zzz', Now());
      if FShowThreads then
        Entry := Format('[%s]-[%-19s%4s] (%.2d) %5s: ', [TimeStamp,SourceFile,SourceLine,LogLevel,FriendlyThreadId]) + Format(Message, vargs) + sLineBreak
      else
        Entry := Format('[%s]-[%-19s%4s] (%.2d) ', [TimeStamp,SourceFile,SourceLine,LogLevel]) + Format(Message, vargs) + sLineBreak;
      if (FLogLevel = LOG_BACKLOG) then
        FBackLog.Add(IntToStr(Integer(LogLevel))+'='+Entry)
      else {if done backlogging then} begin
        // Pascal tries so hard to hide anything pointer-like that you have to use strange and unusual
        // constructs like Pointer(AString)^ in order to pass a pointer to a string's data
        FileWrite(FFileHandle, Pointer(Entry)^, Entry.Length);
        if (FSynchronous) then
          FileFlush(FFileHandle);
        if (FDuplicate) then begin
          FileWrite(StdOutputHandle, Pointer(Entry)^, Entry.Length);
          if (FSynchronous) then FileFlush(StdOutputHandle);
        end;  // if (FDuplicate)
      end;  // if (not backlogging)
    end;  // if (FileHandle <> THandle(-1))
  end;  // if (LogLevel >= Self.LogLevel)
  {$ENDIF}
end;  // procedure TProgramLog.LogEntry(LogLevel: TLogLevels; SourceFile: String; SourceLine: String; Message: String; vargs: Array of const);
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// LogEntry() - Write an entry into the log - alternate entry that simply
//   defaults the LogLevel to LOG_MESSAGE and calls the above
//
procedure TProgramLog.LogEntry(Message: String; vargs: Array of const; SourceFile: String = ''; SourceLine: String = '');
begin
  {$IFNDEF DISABLE_LOGGING}
  LogEntry(LOG_MESSAGE, Message, vargs, SourceFile, SourceLine);
  {$ENDIF}
end;  // procedure TProgramLog.LogEntry(default LogLevel)
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// LogEntry() - Write an entry into the log - alternate entry that allows
//   for a nicely formatted multi-line log entry.  Prints the first line
//   as normal, but with the first Multi (TStrings) entry added on.  Then
//   it prints subsequent lines with the rest of Multi's entries indented to
//   match the first.  If Multi is an empty TStrings then the first line is
//   still printed with just the normal formatting.
//
// Takes:  LogLevel, Message, vargs - See  main LogEntry function
//         Multi:  TStrings with zero or more entries to add to the log
//         SourceFile, SourceLine - See main LogEntry function
//
procedure TProgramLog.LogEntry(LogLevel: TLogLevels; Message: String; vargs: Array of const; Multi: TStrings; SourceFile: String = ''; SourceLine: String = ''); // inline
{$IFNDEF DISABLE_LOGGING}
var
  First, Rest: String;
  n: integer = 1;
  {$ENDIF}
begin
  {$IFNDEF DISABLE_LOGGING}
  if (Multi.Count = 0) then
    Multi.Add('');
  First := Format(Message, vargs) + '%s';
  Rest  := StringOfChar(' ', First.Length-2) + '%s';
  LogEntry(LogLevel, First, [Multi.Strings[0]], SourceFile, SourceLine);
  while (n < Multi.Count) do begin
    LogEntry(LogLevel, Rest, [Multi.Strings[n]], SourceFile, SourceLine);
    inc(n);
  end;  // while (n < Multi.Count)
  {$ENDIF}
end;  // procedure TProgramLog.LogEntry Multi Line entry
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// LogEntry() - Write an entry into the log - alternate entry that allows
//   for a nicely formatted multi-line log entry. Same as above except that
//   Multi is an array of string.
//
procedure TProgramLog.LogEntry(LogLevel: TLogLevels; Message: String; vargs: Array of const; Multi: Array of String; SourceFile: String = ''; SourceLine: String = ''); // inline
{$IFNDEF DISABLE_LOGGING}
var
  TSMulti: TStringList;
  n: Integer = 0;
  {$ENDIF}
begin
  {$IFNDEF DISABLE_LOGGING}
  TSMulti := TStringList.Create;
  while n < Length(Multi) do begin
    TSMulti.Add(Multi[n]);
    inc(n);
  end;
  LogEntry(LogLevel, Message, vargs, TSMulti, SourceFile, SourceLine);
  TSMulti.Free;
  {$ENDIF}
end;  // procedure TProgramLog.LogEntry Other brother Daryl
//---------------------------------------------------------------------------


//  This is for if/when Array of Const args are ever supported for inline functions - if they are then
//  rename the above LogEntry methods as _LogEntry and use these to call them.  Then when you turn off _DEBUG
//  or DISABLE_LOGGING, there is zero impact to your by using LogEntry
//procedure TProgramLog.LogEntry(LogLevel: TLogLevels; Message: String; vargs: Array of const; SourceFile: String = ''; SourceLine: String = '');
//begin
//  {$IFDEF _DEBUG OR $IFDEF ENABLE_LOGGING}
//  _LogEntry(LogLevel, Message, vargs, SourceFile, SourceLine);
//  {$ENDIF}
//end;

//procedure TProgramLog.LogEntry(Message: String; vargs: Array of const; SourceFile: String = ''; SourceLine: String = '');
//begin
//  {$IFDEF _DEBUG OR $IFDEF ENABLE_LOGGING}
//  _LogEntry(Message, vargs, SourceFile, SourceLine);
//  {$ENDIF}
//end;


//---------------------------------------------------------------------------
// FriendlyThreadId: Make a friendlier ThreadId
//
// Takes:   Nothing
// Returns: String holding the current thread ID mod 65536 (to shorten it to
//  something manageably readable on Windows) or MAIN if it's the process'
//  main thread
//
function FriendlyThreadId: String;
var
  Id: TThreadId;
begin
  Id := GetCurrentThreadId();
  if (Id = MainThreadId) then
    Result := 'MAIN'
  else
    Result := (Id mod 65536).ToString();
end;  // function FriendlyThreadId: String;
//---------------------------------------------------------------------------


{the}end.

